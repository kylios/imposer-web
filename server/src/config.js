const DB_USER = process.env.POSTGRES_USER
const DB_PASS = process.env.POSTGRES_PASSWORD
const DB_DB = process.env.POSTGRES_DB
const DB_HOST = process.env.POSTGRES_HOST
const DB_ENGINE = 'postgres'

const config = {
  db: {
    DB_USER,
    DB_PASS,
    DB_DB,
    DB_HOST,
    DB_ENGINE,
  },
  listen: {
    PORT: 3001,
  },
}

export default config