import router from 'koa-router'
import Sequelize from 'sequelize'

import db from '../db.js'
import models from '../models.js'
import { entityToProject, projectToEntity, jobToEntity } from './utils.js'

const Op = Sequelize.Op

const routes = router()

routes.get('/api/v0/project', getProjects)
routes.get('/api/v0/project/:projectId', getProject)
routes.get('/api/v0/project/:projectId/jobs', getProjectJobs)

routes.post('/api/v0/project', postProjects)

routes.put('/api/v0/project/:projectId', putProjects)

routes.delete('/api/v0/project/:projectId', deleteProject)

/**
 * Fetches all non-deleted projects from the database
 * and returns them.
 *
 * API Response:
 * 
 * 200: success
 * 
 * ```
 * [
 *   { ...project entity },
 *   { ...project entity },
 *   { ...project entity },
 *   ...
 * ]
 * ```
 */
async function getProjects(ctx) {
  const projects = await models.Project.findAll()
  const body = projects.map(projectToEntity)

  ctx.body = JSON.stringify(body)
}

/**
 * Fetches a single project from the database and
 * returns it.
 * 
 * API Request:
 *
 * Parameters:
 * `projectId`
 *
 * API Response:
 *
 * 200: success
 * 404: The project does not exist
 *
 * ```
 * {
 *   ...project entity
 * }
 * ```
 */
async function getProject(ctx) {
  const projectId = ctx.params.projectId
  const project = await models.Project.findByPk(projectId)
  if (!project) {
    ctx.throw(404, 'Project does not exist')
  }

  return projectToEntity(project)
}

async function getProjectJobs(ctx) {
  const projectId = ctx.params.projectId

  const project = await models.Project.findByPk(projectId)
  if (!project) {
    ctx.throw(404, 'Project does not exist')
  }

  const jobs = await models.Job.findAll({
    where: {
      projectId
    }
  })

  ctx.body = JSON.stringify(jobs.map(jobToEntity))
}

/**
 * Create a new project in the database and return that
 * project entity.
 *
 * API Request:
 *
 * { ...project entity }
 *
 * API Response:
 *
 * 201: success
 * 400: A required field is missing
 *
 * ```
 * {
 *   ...project entity
 * }
 * ```
 */
async function postProjects(ctx) {
  const body = ctx.request.body
  const project = entityToProject(body)

  if (project.name === '') {
    ctx.throw(400, 'Name cannot be empty')
  } else if (project.description == '') {
    ctx.throw(400, 'Description cannot be empty')
  }
  await project.save()
  
  ctx.status = 201
  ctx.body = JSON.stringify(projectToEntity(project)) 
}

/**
 * Update a project in the database. Returns the updated
 * project.
 *
 * API Request:
 *
 * Parameters:
 * `projectId`
 *
 * { ...project entity }
 *
 * API Response:
 *
 * 200: success
 * 400: A field was missing in the request
 * 404: The project does not exist
 *
 * ```
 * {
 *   ...project entity
 * }
 * ```
 */
async function putProjects(ctx) {
  const projectId = ctx.params.projectId
  const body = ctx.request.body

  if (!body.name || body.name === '') {
    throw "Must set a name"
  } else if (!body.description || body.description === '') {
    throw "Must set description"
  }

  const project = await models.Project.findByPk(projectId)
  if (!project) {
    ctx.throw(404, 'Project does not exist')
  }
  project.name = body.name
  project.description = body.description
  await project.save()

  ctx.body = JSON.stringify(projectToEntity(project))
}

/**
 * Delete the project from the database.
 *
 * API Request:
 *
 * Parameters:
 * `projectId`
 *
 * API Response:
 *
 * 200: success
 * 404: The project does not exist
 */
async function deleteProject(ctx) {
  const projectId = ctx.params.projectId
  const project = await models.Project.findByPk(projectId)
  if (!project) {
    ctx.throw(404, 'Project does not exist')
  }
  await project.destroy()
}

export default routes
