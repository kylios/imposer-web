import models from '../models.js'

/**
 * Converts a job API entity to a job model object.
 * The API entity is not expected to have an `id`, `createTime`,
 * or `endTime` attributes. If any of these attributes are present
 * in the entity, they will not be assigned to the model.
 *
 * @param {object}  entity  The job API entity
 * @param {models.Job} The job model object
 */
export function entityToJob(entity) {
  return models.Job.build({
    projectId: entity.projectId,
    config: entity.config
  })
}

/**
 * Converts a job model object to an API entity.
 *
 * @param {models.Job}  job  The job model object
 * @return {object} An API entity representing the job
 */
export function jobToEntity(job) {
  return {
    id: job.id,
    projectId: job.projectId,
    createTime: job.createTime,
    endTime: job.endTime,
    config: job.config
  }
}

/**
 * Converts a project API entity to a project model object.
 * The API entity is not expected to have an `id`, `createTime`,
 * `updateTime`, or `deleteTime` attributes. If any of these
 * attributes are present in the entity, they will not be
 * assigned to the model.
 *
 * @param {object}  entity  The project API entity
 * @return {models.Project} The project model object
 */
export function entityToProject(entity) {
  return models.Project.build({
    name: entity.name,
    description: entity.description
  })
}

/**
 * Converts a project model object to an API entity.
 *
 * @param {models.Project}  project  The project model object
 * @return {object} An API entity representing the project
 */
export function projectToEntity(project) {
  return {
    id: project.id,
    name: project.name,
    description: project.description,
    createTime: project.createTime,
    updateTime: project.updateTime,
  }
}

/**
 * Ensures the job's configuration is valid and normalizes
 * the configuration. Returns a new job API entity.
 *
 * @param {Context}  ctx  The request context
 * @param {object}   job  The job API entity
 * @return A new copy of the job with a normalized configuration
 */
export function validateJob(ctx, job) {
  const config = {...job.config} || {}

  const integerKeys = {
    signatureSize: {
      required: true,
      min: 1
    },
    pagesPerSheet: {
      required: true,
      min: 1
    },
    signatureMark: {
      required: false
    }
  }

  for (let key in integerKeys) {
    const schema = integerKeys[key]

    if (config[key] === undefined || config[key] === null) {
      if (schema.required) {
        ctx.throw(400, `'${key}' is required`)
      } else {
        config[key] = null
        continue
      }
    }
    if (parseInt(config[key]) === NaN) {
      ctx.throw(400, `'${key}' must be an integer`)
    }
    if (schema.min && config[key] < schema.min) {
      ctx.throw(400, `'${key}' must be at least ${schema.min}`)
    }
  }

  const booleanKeys = {
    twoSided: {
      required: true
    }
  }

  for (let key in booleanKeys) {
    const schema = booleanKeys[key]

    if (config[key] === undefined || config[key] === null) {
      if (schema.required) {
        ctx.throw(400, `'${key}' is required`)
      } else {
        config[key] = null
        continue
      }
    }
    if (config[key] !== true && config[key] !== false) {
      ctx.throw(400, `'${key}' must be boolean`)
    }
  }

  const stringKeys = {
    generateSignatures: {
      required: false
    }
  }

  for (let key in stringKeys) {
    const schema = stringKeys[key]

    if (config[key] === undefined || config[key] === null) {
      if (schema.required) {
        ctx.throw(400, `'${key}' is required`)
      } else {
        config[key] = null
        continue
      }
    }
  }

  return {
    ...job,
    config
  }
}
