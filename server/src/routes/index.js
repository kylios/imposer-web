import projectRoutes from './project.js'
import jobRoutes from './job.js'

function attachToApp(app) {
  app.use(projectRoutes.routes())
  app.use(jobRoutes.routes())
}

export default {
  attachToApp
}
