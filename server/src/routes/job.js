import router from 'koa-router'
import Sequelize from 'sequelize'

import db from '../db.js'
import models from '../models.js'
import { jobToEntity, entityToJob, validateJob } from './utils.js'

const Op = Sequelize.Op

const routes = router()

routes.get('/api/v0/job/:jobId', getJob)

routes.post('/api/v0/job', postJob)

/**
 * Fetches a single job from the database and returns it.
 * 
 * API Request:
 *
 * Parameters:
 * `jobId`
 * 
 * 200: success
 * 404: The job does not exist
 *
 * API Response:
 * 
 * {
 *   ...job entity
 * }
 */
async function getJob(ctx) {
  const jobId = ctx.params.jobId

  const job = await models.Job.findByPk(jobId)
  if (!job) {
    ctx.throw(404, 'Job does not exist')
  }

  ctx.body = JSON.stringify(jobToEntity(job))
}

/**
 * Creates a single job in the database and returns it.
 * 
 * API Request:
 *
 * { ...job entity }
 *
 * API Response:
 * 
 * 201: success
 */
async function postJob(ctx) {
  const body = ctx.request.body
  const job = entityToJob(validateJob(ctx, body))

  await job.save()

  ctx.status = 201
  ctx.body = JSON.stringify(jobToEntity(job))
}

export default routes