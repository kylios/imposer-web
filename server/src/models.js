import Sequelize from 'sequelize'
const Model = Sequelize.Model
import db from './db.js'

class Project extends Model {}
Project.init({
  // The name of this project
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  // A short description of the project
  description: {
    type: Sequelize.STRING,
    allowNull: false,
  },
}, {
  sequelize: db,
  timestamps: true,
})

class Job extends Model {}
Job.init({
  // The project this job belongs to
  projectId: {
    type: Sequelize.INTEGER,
    references: {
      model: Project,
      key: 'id',
    }
  },
  // The timestamp the job ended. If the job was cancelled
  // manually, then `deleteTime` will also be set.
  endTime: {
    type: Sequelize.DATE,
  },
  // The job's configuration
  config: {
    type: Sequelize.JSONB,
  }
}, {
  sequelize: db,
  timestamps: true,
  updatedAt: false,
  deletedAt: false,

  // This is required if `deletedAt` is also `false`
  paranoid: false,
})

export default {
  Project,
  Job,
}
