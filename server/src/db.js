import Sequelize from 'sequelize'
import config from './config.js'

const db = new Sequelize({
  // Connection information
  host: config.db.DB_HOST,
  username: config.db.DB_USER,
  password: config.db.DB_PASS,
  database: config.db.DB_DB,
  dialect: config.db.DB_ENGINE,

  define: {
    createdAt: 'createTime',
    updatedAt: 'updateTime',
    deletedAt: 'deleteTime',
    paranoid: true,
    underscored: true,
  }
})

db
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

export default db