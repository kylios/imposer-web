import koa from 'koa'
import logger from 'koa-logger'
import router from 'koa-router'
import koaBody from 'koa-body'
import config from './config.js'
import db from './db.js'
import models from './models.js'
import routes from './routes/index.js'

async function errorResponse(ctx, next) {
	try {
		await next()
	} catch (exc) {
		ctx.response.body = JSON.stringify({
			error: {
				message: exc.message,
				statusCode: exc.status
			}
		})
		ctx.status = exc.status
	}
}

async function main() {

	const app = new koa()

	app.use(logger())

	const body = koaBody()
	console.log("body", body)

	app.use(errorResponse)
	app.use(body)
	routes.attachToApp(app)

	await db.sync({ force: false })
	app.listen(config.listen.PORT)
}

main()