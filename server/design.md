API Documentation
=====

# Models

## Project

```
{
	"id": <int>,
	"name": <string>,
	"description": <string>,
	"create_time": <timestamp>,
	"update_time": <timestamp>,
	"delete_time": <timestamp> | NULL
}
```

**User-created fields:**

- name
- description

## Job

A new job is created every time a project's files are re-imposed. Each job has its own input files, config, and output files, so all history is retained.

```
{
	"id": <int>,
	"project_id": <int>,
	"create_time": <timestamp>,
	"end_time": <timestamp>,
	"config": <json>	
}
```

**User-created fields:**

- project_id
- config

## JobInputFile

The `name` is a randomized string that will be used to refer to the file via the API. The reason for this is so that files can't be scraped from the site.

They will be ordered by the `order` field.

{
	"id": <int>,
	"job_id": <int>,
	"name": <string>,
	"order": <int>
}

## JobOutputFile

{
	"id": <int>,
	"job_id": <int>,
	"name": <string>,
	"parity": <bool>,
	"signature_number": <int>
}


# API

## Project

GET /project

POST /project

PUT /project

## Job

## JobInputFile

Tracks input files for a job. Files must be in order for their job.

## JobOutputFile



