import React from 'react'
import { Menu } from 'antd'
import {
  ReadOutlined, FileTextOutlined, NumberOutlined
} from '@ant-design/icons'

import { Link } from 'react-router-dom'

export function Sidebar(props) {

  return <Menu
    defaultSelectedKeys={['1']}
    defaultOpenKeys={['sub1']}
    mode="inline"
    theme={"light"}
  >
    <Menu.Item
      display={'Projects'}
      icon={<ReadOutlined />}
      key={1}
    >
      <Link to={'/projects'}>
        {'Projects'}
      </Link>
    </Menu.Item>
    <Menu.Item
      display={'Documentation'}
      icon={<FileTextOutlined />}
      key={2}
    >
      <Link to={`/docs`}>
        {'Documentation'}
      </Link>
    </Menu.Item>
    <Menu.Item
      display={'About'}
      icon={<NumberOutlined />}
      key={3}
    >
      <Link to={`about`}>
        {'About'}
      </Link>
    </Menu.Item>
  </Menu>
}
