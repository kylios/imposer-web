import React from 'react'
import { PageHeader } from 'antd'

export default function Header(props) {

  return <PageHeader
    title={"Imposer"}
    subTitle={"Impose your PDFs for printing as signatures"}
  />
}