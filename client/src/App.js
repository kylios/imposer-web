import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { css } from 'emotion'
import {
  Layout, Breadcrumb
} from 'antd';

import { Sidebar } from './components/Sidebar'
import ProjectPage from './pages/project'
import ProjectJobsPage from './pages/projectJobs'
import JobPage from './pages/job'
import { ProjectProvider } from './api/project'
import { ApiProvider } from './api/api'

import logo from './logo.svg';
import './App.less';

const logoStyle = css`
  color: white;
  height: 64px;
`

function Logo() {
  return <div className={logoStyle}>
    {'Imposer'}
  </div>
}

function App() {
  const [ collapsed, setCollapsed ] = React.useState(false)
  return <ProjectProvider>
    <ApiProvider>
      <Router>
        <Layout style={{minHeight: '100vh'}}>
          <Layout.Sider collapsible collapsed={collapsed} onCollapse={setCollapsed}>
            <Logo />
            <Sidebar />
          </Layout.Sider>
          <Layout className="site-layout">
            <Layout.Header className="site-layout-background" style={{ padding: 0 }}/>
            <Layout.Content style={{ margin: '0 16px' }}>
              <Breadcrumb style={{ margin: '16px 0' }}>

              </Breadcrumb>
              <Switch>
                <Route path={'/projects'}>
                  <ProjectPage />
                </Route>
                <Route path={'/job/:jobId'}>
                  <JobPage />
                </Route>
                <Route path={'/project/:projectId'}>
                  <ProjectJobsPage />
                </Route>
              </Switch>
            </Layout.Content>
          </Layout>
        </Layout>
      </Router>
    </ApiProvider>
  </ProjectProvider>
}

export default App
