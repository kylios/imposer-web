import React from 'react'
import { Descriptions } from 'antd'
import { withRouter } from 'react-router'
import { get } from 'lodash'

import { JobProvider, withJob } from '../api/job'

const JobInfo = withJob(props => {
  const { job, refreshJob } = props

  React.useEffect(() => {
    refreshJob()
  }, [true])

  return <div>
    <Descriptions title={'Job Info'}>
      <Descriptions.Item size={'large'} span={6} label={'Started'}>
        {job.createTime}
      </Descriptions.Item>
      <Descriptions.Item size={'large'} span={6} label={'Ended'}>
        {job.endTime || 'Still Running...'}
      </Descriptions.Item>
    </Descriptions>
    <Descriptions title={'Job Config'} bordered>
      <Descriptions.Item size={'large'} span={2} label={'Signature Size'}>
        {get(job, 'config.signatureSize')}
      </Descriptions.Item>
      <Descriptions.Item size={'large'} span={2} label={'Pages Per Sheet'}>
        {get(job, 'config.pagesPerSheet')}
      </Descriptions.Item>
      <Descriptions.Item size={'large'} span={2} label={'Two Sided'}>
        {get(job, 'config.twoSided') ? 'Yes' : 'No'}
      </Descriptions.Item>
      <Descriptions.Item size={'large'} span={2} label={'Signature Mark'}>
        {get(job, 'config.signatureMark')}
      </Descriptions.Item>
      <Descriptions.Item size={'large'} span={2} label={'Generate Signatures'}>
        {get(job, 'config.generateSignatures')}
      </Descriptions.Item>
    </Descriptions>
  </div>
})

const JobPage = withRouter((props) => {
  const { match } = props
  const jobId = parseInt(match.params.jobId)

  return <JobProvider jobId={jobId}>
    <JobInfo /> 
  </JobProvider>
})


export default JobPage