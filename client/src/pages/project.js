import React from 'react'
import { Space, Descriptions, Input, Button, Table, Collapse } from 'antd'
import { ProjectProvider, withProject } from '../api/project'
import { Link } from 'react-router-dom'

const AddProject = withProject(
  ({addProject}) => {
    const [name, setName] = React.useState('')
    const [description, setDescription] = React.useState('')

    return <Collapse bordered={false} defaultActiveKey={['1']}>
      <Collapse.Panel header={'Create a New Project'}>
        <Space direction={'vertical'} size={8}>
          <Space direction={'vertical'}>
            <Descriptions.Item size={'large'} label={'Name'}>
              {'Name'}
            </Descriptions.Item>
            <Input
              size={'large'}
              placeholder={'Enter the project name'}
              onChange={e => setName(e.target.value)}
              value={name}
            />
          </Space>
          <Space direction={'vertical'}>
            <Descriptions.Item size={'large'} label={'Description'}>
              {'Description'}
            </Descriptions.Item>
            <Input
              size={'large'}
              placeholder={'Simple project description'}
              onChange={e => setDescription(e.target.value)}
              value={description}
            />
          </Space>
          <Button
            type={'primary'}
            onClick={() => {
              addProject({name, description})
              setName('')
              setDescription('')
            }}
          >
            {'Add Project'}
          </Button>
        </Space>
      </Collapse.Panel>
    </Collapse>
  })

const ProjectList = withProject(
  ({projects}) => {

    const columns = [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
      }
    ]

    const dataSource = Object.values(projects).map(
      (project, i) => ({
        key: `${i}`,
        name: <Link to={`/project/${project.id}`}>{project.name}</Link>,
        description: project.description
      })
    )

    return <div>
      <h3>Projects</h3>
      <Table columns={columns} dataSource={dataSource} />
    </div>
  })


const ProjectPage = withProject((props) => {

  const { refreshProjects } = props

  React.useEffect(() => {
    refreshProjects()
  }, [true])

  return <div className="App">
    <Space direction={'vertical'} size={16} style={{width: '100%'}}>
      <AddProject />
      <ProjectList />
    </Space>
  </div>
})

export default ProjectPage