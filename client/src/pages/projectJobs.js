import React from 'react'
import { withRouter } from 'react-router'
import { Descriptions, Input, Checkbox, Space, Col, Table, Button } from 'antd'
import { object, number, array } from 'yup'
import { css } from 'emotion'
import { Link } from 'react-router-dom'
import { ProjectJobProvider, withProjectJob } from '../api/projectJob'
import { jobUrl } from '../api/utils'


// function coerceToType(typ, val) {
//   if (typ === 'int') {
//     const newVal = parseInt(val)
//     if (newVal === NaN) {
//       throw Exception(`'${val} is not an integer`)
//     }
//     return newVal
//   } else {
//     return val
//   }
// }


// function validateAndSave(schema, data) {
//   const newData = {}
//   const errors = {}
//   for ([key, typ] in Object.entries(schema)) {
//     try {
//       const val = coerceToType(typ, data[key])
//       newData[key] = val
//     } catch (exc) {

//     }
//   }
// }


const AddJob = withProjectJob(
  ({addJob, projectId}) => {

    const schema = {
      signatureSize: 'int',
      pagesPerSheet: 'int',
      signatureMark: 'int',
      generateSignatures: 'str',
      twoSided: 'bool'
    }

    const [signatureSize, setSignatureSize] = React.useState('')
    const [pagesPerSheet, setPagesPerSheet] = React.useState('')
    const [signatureMark, setSignatureMark] = React.useState('')
    const [generateSignatures, setGenerateSignatures] = React.useState('')
    const [twoSided, setTwoSided] = React.useState(true)

    const setDefaults = () => {
      setSignatureSize('')
      setPagesPerSheet('')
      setSignatureMark('')
      setGenerateSignatures('')
      setTwoSided(true)
    }

    return <div className={css`
      flex-grow: 1;
      min-width: 300px;
    `}>
      <h3>{'Add a Job'}</h3>
      <Space direction={'vertical'} size={8}>
        <Space direction={'vertical'}>
          <Descriptions.Item size={'large'} label={'Signature Size'}>
            {'The number of folios per signature.'}
          </Descriptions.Item>
          <Input
            size={'large'}
            placeholder={'i.e. 4'}
            onChange={e => setSignatureSize(e.target.value)}
            value={signatureSize}
          />
        </Space>
        <Space direction={'vertical'}>
          <Descriptions.Item size={'large'} label={'Two Sided'}>
            {'Two Sided'}
          </Descriptions.Item>
          <Checkbox
            checked={twoSided}
            onChange={e => setTwoSided(e.target.checked)}
          />
        </Space>
        <Space direction={'vertical'}>
          <Descriptions.Item size={'large'} label={'Pages Per Sheet'}>
            {'The number of pages to fit on a single sheet.'}
          </Descriptions.Item>
          <Input
            size={'large'}
            placeholder={'i.e. 2'}
            onChange={e => setPagesPerSheet(e.target.value)}
            value={pagesPerSheet}
          />
        </Space>
        <Space direction={'vertical'}>
          <Descriptions.Item size={'large'} label={'Signature Mark'}>
            {'The width (in pixels) of the signature mark. Leave blank for none.'}
          </Descriptions.Item>
          <Input
            size={'large'}
            placeholder={'i.e. 5'}
            onChange={e => setSignatureMark(e.target.value)}
            value={signatureMark}
          />
        </Space>
        <Space direction={'vertical'}>
          <Descriptions.Item size={'large'} label={'Generate Signatures'}>
            {'Which signatures to generate, separated by a comma. Signature numbers start at 0. Leave blank for all.'}
          </Descriptions.Item>
          <Input
            size={'large'}
            placeholder={'i.e. 0,1,2,3,4'}
            onChange={e => setGenerateSignatures(e.target.value)}
            value={generateSignatures}
          />
        </Space>
        <Button
            type={'primary'}
            onClick={() => {
              addJob({
                config: {
                  signatureSize: parseInt(signatureSize),
                  twoSided: !!twoSided,
                  pagesPerSheet: parseInt(pagesPerSheet),
                  signatureMark: parseInt(signatureMark),
                  generateSignatures: generateSignatures.length ? generateSignatures : null
                }
              })
              setDefaults()
            }}
          >
            {'Add Project'}
          </Button>
      </Space>
    </div>
  })

const JobList = withProjectJob(
  ({projectId, jobs, refreshJobs}) => {

    React.useEffect(() => {
      refreshJobs()
    }, [true])

    const columns = [
      {
        title: 'Created',
        dataIndex: 'createTime',
        key: 'createTime'
      },
      {
        title: 'Finished',
        dataIndex: 'endTime',
        key: 'endTime'
      }
    ]

    const dataSource = Object.values(jobs).map(
      (job, i) => ({
        key: `${i}`,
        createTime: <Link to={`/job/${job.id}`}>
          {job.createTime}
        </Link>,
        endTime: job.endTime
      })
    )

    return <div className={css`
      min-width: 320px;
    `}>
      <h3>Job History</h3>
      <Table columns={columns} dataSource={dataSource} />
    </div> 
  })


const ProjectJobsPage = withRouter((props) => {
  const { match } = props
  const projectId = parseInt(match.params.projectId)

  return <ProjectJobProvider projectId={projectId}>
    <div className={css`
      display: flex;
    `}>
      <AddJob />
      <JobList />
    </div>
  </ProjectJobProvider>
})

export default ProjectJobsPage
