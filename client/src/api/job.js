import React from 'react'

import { noop, API_REQUEST_OPTIONS, jobUrl } from './utils'
import { withApi } from './api'

const JobContext = React.createContext({
  job: {}
})

export function JobProvider(props) {
  const {
    children,
    jobId
  } = props

  const [job, setJob] = React.useState({})

  return <JobContext.Provider value={{jobId, job, setJob}}>
    {children}
  </JobContext.Provider>
}

const WithJob = withApi((myProps) => {
  const {
    jobId,
    job,
    setJob,
    Component,
    apiGet,
    props
  } = myProps

  const refreshJob = () => {
    apiGet(jobUrl(jobId))
      .on(2, async (resp) => {
        const body = await resp.json()
        setJob(body)
      })
  }

  return <Component
    {...props}
    jobId={jobId}
    job={job}
    refreshJob={refreshJob}
  />
})

export function withJob(Component) {
  return props => <JobContext.Consumer>{
    (value) => {
      return <WithJob
        jobId={value.jobId}
        Component={Component}
        job={value.job}
        setJob={value.setJob}
        props={props}
      />
    }
  }</JobContext.Consumer>
}