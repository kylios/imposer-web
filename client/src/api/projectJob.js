import React from 'react'

import { noop, API_REQUEST_OPTIONS, projectJobsUrl, jobUrl } from './utils'
import { withApi } from './api'

const ProjectJobContext = React.createContext({
  jobs: {},
  setJobs: noop
})

export function ProjectJobProvider(props) {
  const {
    children,
    projectId
  } = props

  const [jobs, setJobs] = React.useState({})

  return <ProjectJobContext.Provider value={{jobs, setJobs, projectId}}>
    {children}
  </ProjectJobContext.Provider>
}

const WithProjectJob = withApi((myProps) => {
  const {
    projectId,
    jobs,
    setJobs,
    Component,
    apiGet,
    apiPost,
    props
  } = myProps

  const refreshJobs = () => {
    apiGet(projectJobsUrl(projectId))
      .on(2, async (resp) => {
        const body = await resp.json()
        const projectJobs = body.reduce(
          (acc, val) => ({
            ...acc,
            [val.id]: val
          }),
          {}
        )
        setJobs(projectJobs)
      })
  }

  const addJob = job => {
    apiPost(jobUrl(), {
        ...job,
        projectId
      })
      .on(2, async (resp) => {
        const body = await resp.json()
        setJobs({
          ...jobs,
          [body.id]: body
        })

        return body
      })
  }

  return <Component
    {...props}
    projectId={projectId}
    jobs={jobs}
    refreshJobs={refreshJobs}
    addJob={addJob}
  />
})

export function withProjectJob(Component) {
  return props => <ProjectJobContext.Consumer>{
    (value) => {
      return <WithProjectJob
        projectId={value.projectId}
        Component={Component}
        jobs={value.jobs}
        setJobs={value.setJobs}
        props={props}
      />
    }
  }</ProjectJobContext.Consumer>
}
