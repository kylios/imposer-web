export const API_REQUEST_OPTIONS = {
  headers: {
    'Content-Type': 'application/json',
  },
  cache: 'no-cache',
}

export const projectUrl = projectId =>
  projectId ? `/api/v0/project/${projectId}` : '/api/v0/project'
export const projectJobsUrl = (projectId) => `${projectUrl(projectId)}/jobs`
export const jobUrl = (jobId) => jobId ? `/api/v0/job/${jobId}` : '/api/v0/job'

export const noop = () => null
