import React from 'react'
import { Modal } from 'antd'

import { noop, API_REQUEST_OPTIONS } from './utils'

/**
 * This class encapsulates a promise which holds a `Response`
 * object generated from a call to `fetch`. The class allows
 * the caller to easily specify different callbacks depending
 * on the status code of the response.
 *
 * Example:
 * 
 * ```
 * new ApiCall(fetch(someUrl))
 *   .on(2, handleSuccess)
 *   .on(301, handleRedirect)
 *   .on(4, handleBadRequest)
 *   .on(409, handleConflict)
 *   .on(5, handleServerError)
 *   .catch(handleRequestError)
 * ```
 */
class ApiCall {
  constructor(promise) {
    this._promise = promise
    this._handlers = {}

    this._promise.then(this._handleResponse)
  }

  /**
   * Handles a successful `fetch` response and invoke the
   * appropriate handler callback.
   */
  _handleResponse = result => {
    const status = result.status
    const range = parseInt(result.status / 100)
    if (status in this._handlers) {
      return this._handlers[status](result)
    } else if (range in this._handlers) {
      return this._handlers[range](result)
    }
  }

  /**
   * Sets a callback for the given status code or status
   * code range.
   * 
   * An exact status code has precedence over status code
   * ranges. For example:
   *
   * ```
   * new ApiCall(fetch(someUrl))
   *   .on(4, handleClientError)
   *   .on(403, handleUnauthorized)
   * ```
   *
   * This code would call `handleUnauthorized` if a 403 error
   * was returned, but would call `handleClientError` if
   * any other 400-level error status code were returned. The
   * order in which the `.on` method is called does not matter.
   */
  on = (status, callback) => {
    this._handlers[status] = callback
    return this
  }

  /**
   * Sets an error handling callback.
   */
  catch = callback => {
    this._promise.catch(callback)
    return this
  }
}

/**
 * Holds the API functions and error status.
 */
const ApiContext = React.createContext({
  apiCall: noop
})


/**
 * Issues a GET request.
 */
function apiGet(url) {
  return fetch(url, API_REQUEST_OPTIONS)
}

/**
 * Issues a POST request.
 */
function apiPost(url, body) {
  return fetch(url, {
    ...API_REQUEST_OPTIONS,
    method: 'POST',
    body: JSON.stringify(body)
  })
}

/**
 * Issues a PUT request.
 */
function apiPut(url, body) {
  return fetch(url, {
    ...API_REQUEST_OPTIONS,
    method: 'PUT',
    body: JSON.stringify(body)
  })
}

/**
 * Issues a DELETE request.
 */
function apiDelete(url) {
  return fetch(url, {
    ...API_REQUEST_OPTIONS,
    method: 'DELETE'
  })
}

/**
 * Provides the api methods and error state.
 */
function ApiProvider(props) {
  const { children } = props

  const [modal, contextHolder] = Modal.useModal()

  const displayError = async (response) => {
    const body = await response.json()

    modal.error({
      title: 'Error',
      content: <div>
        {`${body.error.statusCode}: ${body.error.message}`}
      </div>
    })
  }

  const wrapApiCall = (func) => (...args) => {
    const promise = func(...args)
    const api = new ApiCall(promise)
      .catch(displayError)
      .on(4, displayError)
      .on(5, displayError)

    return api
  }

  // These are the props that will be passed to the
  // component wrapped by `withApi`
  const value = {
    apiGet: wrapApiCall(apiGet),
    apiPost: wrapApiCall(apiPost),
    apiPut: wrapApiCall(apiPut),
    apiDelete: wrapApiCall(apiDelete)
  }

  return <ApiContext.Provider value={value}>
    {children}
    {contextHolder}
  </ApiContext.Provider>
}

function WithApi(myProps) {
  const {
   Component,
   props,
   ...rest
  } = myProps

  return <Component
    {...props}
    {...rest}
  />
}

/**
 * Wraps a component, passing as props all api methods and
 * error state.
 */
function withApi(Component) {
  return props => <ApiContext.Consumer>{
    value => <WithApi
      Component={Component}
      props={props}
      {...value}
    />
  }</ApiContext.Consumer>
}

export { withApi, ApiProvider }