import React from 'react'

import { noop, API_REQUEST_OPTIONS, projectUrl } from './utils'
import { withApi } from './api'

const ProjectContext = React.createContext({
  projects: {},
  setProjects: noop
})

export function ProjectProvider(props) {
  const {
    children
  } = props

  const [projects, setProjects] = React.useState({})

  return <ProjectContext.Provider value={{projects, setProjects}}>
    {children}
  </ProjectContext.Provider>
}

const WithProject = withApi((myProps) => {
  const {
    projects,
    setProjects,
    Component,
    apiGet,
    apiPost,
    apiPut,
    apiDelete,
    props
  } = myProps

  const refreshProjects = () => {
    apiGet(projectUrl())
      .on(2, async (resp) => {
        const body = await resp.json()
        const projects = body.reduce(
          (acc, val) => ({
            ...acc,
            [val.id]: val
          }),
          {}
        )
        setProjects(projects)
      })
  }

  const addProject = project => {
    apiPost(projectUrl(), project)
      .on(2, async (resp) => {
        const body = await resp.json()
        setProjects({
          ...projects,
          [body.id]: body
        })
      })
  }

  const updateProject = project => {
    apiPut(projectUrl(project.id), project)
      .on(2, async (resp) => {
        const body = await resp.json()
        setProjects({
          ...projects,
          [body.id]: body
        })
      })
  }

  const removeProject = projectId => {
    apiDelete(projectUrl(projectId))
      .on(2, async (resp) => {
        if (!(projectId in projects)) {
          return
        }

        const newProjects = {}
        for (let id in projects) {
          if (parseInt(id) !== projectId) {
            newProjects[parseInt(id)] = projects[id]
          }
        }
        setProjects(newProjects)
      })
  }

  return <Component
    {...props}
    projects={projects}
    refreshProjects={refreshProjects}
    addProject={addProject}
    removeProject={removeProject}
    updateProject={updateProject}
  />
})

export function withProject(Component) {
  return props => <ProjectContext.Consumer>{
    (value) => {
      return <WithProject
        Component={Component}
        projects={value.projects}
        setProjects={value.setProjects}
        props={props}
      />
    }
  }</ProjectContext.Consumer>
}
